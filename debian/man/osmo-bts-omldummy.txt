NAME
  osmo-bts-omldummy - operational maintenance link

SYNOPSIS
  osmo-bts-omldummy [prog-name]

DESCRIPTION
  Find documentation for the osmo-bts project at
.UR "https://downloads.osmocom.org/docs/osmo-bts/"
.UE

OPTIONS
  -h,--help                 this text
  -f, --features            Usage: program-name [-h] [--features FOO,BAR,BAZ] dst_host site_id [trx_num]


SEE ALSO
  osmo-trx-uhd(1), osmo-bsc(8)

AUTHOR
  This manual page was written by Debian MobCom Team for the Debian project (and may be used by others).
