#!/bin/bash


txt2man -d "${CHANGELOG_DATE}" -t OSMO-BTS-TRX            -s 8 osmo-bts-trx.txt          > osmo-bts-trx.8
txt2man -d "${CHANGELOG_DATE}" -t OSMO-BTS-OMLDUMMY       -s 8 osmo-bts-omldummy.txt     > osmo-bts-omldummy.8
txt2man -d "${CHANGELOG_DATE}" -t OSMO-BTS-VIRTUAL        -s 8 osmo-bts-virtual.txt      > osmo-bts-virtual.8
