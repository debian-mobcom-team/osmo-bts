Source: osmo-bts
Maintainer: Debian Mobcom Maintainers <Debian-mobcom-maintainers@lists.alioth.debian.org>
Uploaders: Ruben Undheim <ruben.undheim@gmail.com>,
           Thorsten Alteholz <debian@alteholz.de>
Section: net
Priority: optional
Build-Depends: debhelper-compat (= 13),
               pkgconf,
               libosmocore-dev (>= 1.9.0),
               libosmo-abis-dev (>= 1.5.0),
               libgps-dev,
               libortp-dev,
               libosmocoding0,
               libosmo-netif-dev (>= 1.4.0),
               txt2man
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-mobcom-team/osmo-bts
Vcs-Git: https://salsa.debian.org/debian-mobcom-team/osmo-bts.git
Homepage: http://openbsc.osmocom.org/trac/wiki/OsmoBTS
Rules-Requires-Root: no

Package: osmo-bts
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: osmo-bsc
Description: Base Transceiver Station for GSM
 OsmoBTS is a software implementation of Layer2/3 of a BTS. It implements the
 following protocols/interfaces:
    LAPDm (GSM 04.06)
    RTP
    A-bis/IP in IPA multiplex
    OML (GSM TS 12.21)
    RSL (GSM TS 08.58)
 .
 OsmoBTS is modular and has support for multiple back-ends. A back-end talks to
 a specific L1/PHY implementation of the respective BTS hardware. Based on this
 architecture, it should be relatively easy to add a new back-end to support
 so-far unsupported GSM PHY/L1 and associated hardware.
